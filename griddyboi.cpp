#include <iostream>
#include <fstream>
#include <string>
using namespace std;

struct Color
{
    int r, g, b;
    Color( int r, int g, int b )
    {
        this->r = r;
        this->g = g;
        this->b = b;
    }
};

int main( int argc, char** argv )
{
    int tileWidth = 32;
    int tileHeight = 32;
    int gridWidth = 10;
    int gridHeight = 10;
    int imageWidth;
    int imageHeight;
    
    Color c1( 255, 0, 255 );
    Color c2( 0, 255, 255 );
    
    cout << "Tile width height: ";
    cin >> tileWidth >> tileHeight;
    
    cout << "1. Grid width/height, or 2. Image width/height? ";
    int choice;
    cin >> choice;
    
    if ( choice == 1 )
    {
        cout << "Grid width height: ";
        cin >> gridWidth >> gridHeight;
        
        imageWidth = tileWidth * gridWidth;
        imageHeight = tileHeight * gridHeight;
    }
    else
    {
        cout << "Image width height: ";
        cin >> imageWidth >> imageHeight;
        gridWidth = imageWidth / tileWidth;
        gridHeight = imageHeight / tileHeight;
    }
    
    cout << endl;
    
    cout << "Tile dimensions:  " << tileWidth << "x" << tileHeight << endl;
    cout << "Grid dimensions:  " << gridWidth << "x" << gridHeight << endl;
    cout << "Image dimensions: " << imageWidth << "x" << imageHeight << endl;
    
    ofstream output( "grid.ppm" );
    output << "P3" << endl << "# Moosadee Grid :)" << endl << imageWidth << " " << imageHeight << endl << "255" << endl;
    
    // Make pixels
    for ( int y = 0; y < imageHeight; y++ )
    {
        for ( int x = 0; x < imageWidth; x++ )
        {
            output << "# Tile (" << x / tileWidth << ", " << y % tileWidth <<")" << endl;
            
            bool x1 = (x % (tileWidth*2) < 32);
            bool y1 = (y % (tileWidth*2) < 32);
            
            if ( (x1 && y1) || (!x1 && !y1) )
            {
                output << c1.r << endl << c1.g << endl << c1.b << endl;
            }
            else if ( (x1 && !y1) || (!x1 && y1) )
            {
                output << c2.r << endl << c2.g << endl << c2.b << endl;
            }
        }
    }
    
    return 0;
}
